# DN Test Helpers

A Clojure library designed to assist with Clojure and Clojurescript unit testing.
This includes any Hiccup-style structures that need to be verified.

## Usage

Include the library in your project, probably under the `:dev` profile:

```clojure
[dn/test-helpers "0.1.0-SNAPSHOT"]
```

Then in your unit test, you can verify Hiccup-style structures.  Suppose you have a function that generates
something like this:

```clojure
[:div.test-component
 [:h1 "Test title"]
 [:button {:on-click (constantly "ok")} "Test button"]]
```

You can test it like this:

```clojure
(require '[dn.test-helpers.hiccup :as h])

;; Assuming you're using Midje
(facts "about test component"
  (fact "it's a html tag"
    (test-component) => h/html-tag?)
    
  (fact "contains a title"
    (->> (test-component)
         (h/find-tag :h1)) => truthy)

  (fact "button has a handler"
    (->> (test-component)
         (h/find-tag :button)
	 (h/attributes)
	 :on-click) => fn?))
```

There is also a `dn.test-helpers.checkers` namespace that contains Midje checkers:

```clojure
(require '[dn.test-helpers.checkers :as c])
...
(test-component) => c/html-element?
;; Or as an argument
(something) => some-validation
(provided (a-fn-that-accepts-element c/html-element?) => ..some-reply..)
...
```

## TODO

Expand Hiccup functionality, also add other test helpers as needed.

## License

Copyright © 2019-2020 Debreuck Neirynck

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
