(ns dn.test-helpers.hiccup
  "Checkers and other helper functions for hiccup-style structures"
  (:require [clojure.walk :as w]))

(defn- take-next-element-part [x]
  (let [f (if (or (.startsWith x ".") (.startsWith x "#"))
            (str (first x))
            "")]
    (some->> (clojure.string/split x #"\.|#")
             (drop-while empty?)
             (first)
             (str f))))

(defn extract-element
  "Given a keyword, extracts the html element from it by separating it from the optional classes and id"
  [x]
  (when (keyword? x)
    (some-> x
            (name)
            (take-next-element-part)
            (keyword))))

(defn parse-element
  "Given a keyword, splits it into the tag name, and (optional) classes and id.  E.g. `div.footer#main-footer`
   would result in `{:tag :div :classes [:footer] :id :main-footer}`"
  [el]
  (when (keyword? el)
    (let [parts (loop [s (name el)
                       res []]
                  (if (empty? s)
                    res
                    (let [r (take-next-element-part s)]
                      (recur (subs s (count r))
                             (conj res r)))))
          {ids \# classes \.} (group-by #(.charAt % 0) (rest parts))
          conv (comp keyword #(subs % 1))]
      {:tag (keyword (first parts))
       :id (first (map conv ids))
       :classes (mapv conv classes)})))

(defn- classes->str [c]
  (->> c
       (map name)
       (clojure.string/join " ")))

(defn normalize-tag
  "Takes a tag vector and normalizes it by parsing the element and adding any classes and id to the attributes"
  [t]
  (let [{:keys [tag id classes]} (parse-element (first t))]
    (cond
      ;; No id and no classes, so already normalized
      (and (nil? id) (empty? classes)) t
      ;; Has existing attributes
      (map? (second t)) (vec (concat [tag
                                      (merge (second t)
                                             {:id id :class (classes->str classes)})]
                                     (drop 2 t)))
      ;; Has no attributes yet
      :else (vec (concat [tag {:id id :class (classes->str classes)}] (rest t))))))

(defn element-matcher
  "Creates a fn that verifies that the given vector is the specified html element"
  [el]
  (fn [v] (= el (extract-element (first v)))))

(defn- as-matcher
  "If x is a keyword or a non-fn, returns equality function for x, otherwise return x"
  [x]
  (if (or (keyword? x) (not (fn? x)))
    (comp (partial = x) first)
    x))

(defn attributes
  "Extracts any html attributes from the tag"
  [tag]
  (let [s (second tag)]
    (when (map? s) s)))

(defn has-attribute?
  "Checks if `tag` contains attribute matched by `m`.  `m` can be a value, in which
   case the attribute name is matched against it, or a function that should accept
   the vector of `[attr-key attr-val]`.  Returns the first match, or `nil`."
  [tag m]
  (->> (attributes tag)
       (filter (as-matcher m))
       (first)))

(defn- id-from-key [k]
  (some-> k
          (name)
          (clojure.string/split #"#")
          (second)
          (clojure.string/split #"\.")
          (first)
          (keyword)))

(defn tag-id
  "Given a html tag, retrieves its id"
  [tag]
  (or (-> tag (attributes) :id)
      (id-from-key (first tag))))

(defn html-tag? [x]
  (and (vector? x)
       (keyword? (first x))))

(defn sub-component?
  "True if the given object is a component (i.e. a vector with a function as first element)"
  [x]
  (and (vector? x)
       ;; Use `ifn` for multimethods
       (ifn? (first x))))

(defn subtags
  "Returns a list of subtags of given tag"
  [x]  
  (->> (rest x)
       ;; Skip attributes map
       (drop-while map?)
       (filter (some-fn html-tag? sub-component?))))

(defn find-all
  "Finds all tags that match the matcher in the given html tree"
  [matcher tree]
  (->> (tree-seq
        (fn [x] (and (html-tag? x)
                     (not-empty (subtags x))))
        subtags
        tree)
       (filter matcher)))

(defn find-tag
  "Finds the first match in the tree for given matcher function.  If the matcher is a keyword,
   then it will look for the html tag with given key."
  [matcher tree]
  (when (html-tag? tree)
    ;; TODO Use as-matcher here
    (let [m (if (keyword? matcher) (element-matcher matcher) matcher)]
      (first (find-all m tree)))))

(defn find-tag-by-id
  "Tries to find the element with given id"
  [id tree]
  (find-tag (comp (partial = id) tag-id) tree))

(defn classes
  "Returns the classes given tag belongs to"
  [tag]
  (some-> tag
          (normalize-tag)
          (attributes)
          :class
          (clojure.string/split #" ")))

(defn has-class?
  "Checks if given tag belongs to the given class."
  [tag cl]
  (contains? (set (classes tag)) cl))
