(ns dn.test-helpers.checkers
  "Midje-style checkers"
  (:require [midje.sweet :refer :all]
            [dn.test-helpers.hiccup :as h]))

(defchecker html-element?
  "Checker that verifies that the given vector is the specified html element"
  [el]
  (let [m (h/element-matcher el)]
    (checker [v] (m v))))
