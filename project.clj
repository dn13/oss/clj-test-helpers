(defproject dn/test-helpers "0.1.1-SNAPSHOT"
  :description "Helper functions for unit tests"
  :url "https://gitlab.com/dn13/oss/clj-test-helpers"
  :license {:name "Eclipse Public License 2"
            :url "https://opensource.org/licenses/EPL-2.0"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [midje "1.9.9"]]
  :profiles {:dev {:dependencies [[dn/midje-junit-formatter "0.1.1"]]
                   :plugins [[lein-midje "3.2.1"]]}}
  :aliases {"autotest" ["midje" ":autotest"]
            "gitlab" ["midje" ":config" "midje-gitlab.clj"]}
  :source-paths ["src"]
  :deploy-repositories [["dn-clojars" {:url "https://clojars.org/repo"
                                       :username [:gpg :env/clojars_user]
                                       :password [:gpg :env/clojars_pass]}]])
