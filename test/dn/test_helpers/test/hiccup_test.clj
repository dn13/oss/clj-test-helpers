(ns dn.test-helpers.test.hiccup-test
  (:require [midje.sweet :refer :all]
            [dn.test-helpers.hiccup :refer :all]))

(facts "about `parse-element`"
       (fact "parses element without id or classes"
             (parse-element :div) => (contains {:tag :div}))

       (fact "parses element with id"
             (parse-element :div#test-id) => (contains {:tag :div
                                                        :id :test-id}))

       (fact "parses classes"
             (parse-element :div.a.b) => (contains {:tag :div
                                                    :classes [:a :b]})))

(facts "about `normalize-tag`"
       (fact "returns normalized as-is"
             (normalize-tag
              [:div {:id "test-id"
                     :class "a b"} "test"]) => [:div {:id "test-id"
                                                      :class "a b"} "test"])
       
       (fact "extracs id and classes into attributes"
             (normalize-tag
              [:div#test-id.a.b "test"]) => [:div {:id :test-id
                                                   :class "a b"} "test"])

       (fact "updates existing attributes"
             (normalize-tag
              [:div#test-id.a.b {:alt "something"} "test"]) => [:div {:id :test-id
                                                                      :class "a b"
                                                                      :alt "something"} "test"])

       (future-fact "adds classes to existing classes"))
                                                    
(facts "about `attributes`"
       (fact "nil if no tag"
             (attributes nil) => nil?
             (attributes []) => nil?)

       (fact "returns attributes from tag"
             (attributes [:a {:href "test"}]) => {:href "test"})

       (fact "nil if no attributes in tag"
             (attributes [:p "This is a test"]) => nil?))

(facts "about `has-attribute?`"
       (fact "falsey if no attribute"
             (has-attribute? [:div {}] :test) => falsey)
       
       (fact "matches keyword attr"
             (has-attribute? [:div {:test "value"}] :test) => truthy)
       
       (fact "applies matcher fn"
             (has-attribute? [:div {:test "value"}] (comp (partial = "value") second)) => truthy))

(facts "about `tag-id`"
       (fact "nil if no id"
             (tag-id nil) => nil?
             (tag-id [:p "test"]) => nil?
             (tag-id [:p.test "test"]) => nil?)

       (fact "returns id from tag attributes"
             (tag-id [:p {:id :test} "test"]) => :test)

       (fact "returns id from tag key"
             (tag-id [:p#test-id "test"]) => :test-id
             (tag-id [:p.some-class#test-id "test"]) => :test-id
             (tag-id [:p#test-id.some-class "test"]) => :test-id))

(facts "about `html-tag?`"
       (fact "false if nil"
             (html-tag? nil) => falsey)

       (fact "false if no tag"
             (html-tag? "test") => falsey
             (html-tag? {}) => falsey
             (html-tag? []) => falsey
             (html-tag? ["test"]) => falsey)

       (fact "true if tag"
             (html-tag? [:p "Test"]) => truthy))

(defn- test-component []
  [:div "test"])

(facts "about `subtags`"
       (fact "empty if no subtags"
             (subtags nil) => empty?
             (subtags []) => empty?
             (subtags [:div]) => empty?
             (subtags [:div {:id "test"}]) => empty?)

       (fact "returns subtags collection for tag"
             (subtags [:div [:p "first"] [:p "second"]]) => [[:p "first"] [:p "second"]])

       (fact "includes function components as subtag"
             (subtags [:div [test-component]]) => [[test-component]])

       (fact "skips non-tag children"
             (subtags [:p "test paragraph"]) => empty?))

(facts "about `find-tag`"
       (fact "nil if no tag"
             (find-tag :h1 nil) => nil?)

       (fact "returns first occurrance in top level"
             (let [t [:p "test"]]
               (find-tag :p t) => t
               (find-tag :other t) => nil?))

       (fact "returns match in tree"
             (let [t [:div
                      [:h1 "Title"]
                      [:p "Paragraph"]
                      [:div
                       {:class "test"}
                       [:h2 "Subtitle"]]]]
               (find-tag :h1 t) => [:h1 "Title"]
               (find-tag :h2 t) => [:h2 "Subtitle"]
               (find-tag :other t) => nil?)))

(facts "about `find-tag-by-id`"
       (fact "nil if empty tree"
             (find-tag-by-id nil :test) => nil?
             (find-tag-by-id [] :test) => nil?)

       (fact "finds tag with given id"
             (let [tree [:div#top
                         [:div#child-1]
                         [:div#child-2
                          [:div#grandchild-1]]]]
               (find-tag-by-id :top tree) => tree
               (find-tag-by-id :child-1 tree) => [:div#child-1]
               (find-tag-by-id :grandchild-1 tree) => [:div#grandchild-1])))

(facts "about `find-all`"
       (fact "empty if empty tag"
             (find-all some? nil) => empty?)
       
       (fact "empty if no matches"
             (find-all #(has-class? % :test) [:div.other]) => empty?)

       (fact "finds all matches in tree on same level"
             (->> [:div
                   [:p "First paragraph"]
                   [:p "Second paragraph"]]
                  (find-all (element-matcher :p))
                  (count)) => 2)

       (fact "finds all matches in multiple levels"
             (->> [:div
                   [:p "First paragraph"]
                   [:div.child
                    [:p "Child paragraph"]]]
                  (find-all (element-matcher :p))
                  (count)) => 2))

(facts "about `classes`"
       (fact "empty if no attributes"
             (classes [:div]) => empty?)
       
       (fact "empty if no classes"
             (classes [:div {:id "test"}]) => empty?)

       (fact "returns class names"
             (classes [:div {:class "a b"}]) => ["a" "b"])

       (fact "returns class names when in tag key"
             (classes [:div.a.b]) => ["a" "b"]))

(facts "about `has-class?`"
       (fact "falsey if no match"
             (has-class? [:div] "test") => falsey
             (has-class? [:div {}] "test") => falsey
             (has-class? [:div {:class "other"}] "test") => falsey)

       (fact "truthy if match"
             (has-class? [:div {:class "other test"}] "test") => truthy))
