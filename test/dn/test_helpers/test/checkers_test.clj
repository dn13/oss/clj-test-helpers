(ns dn.test-helpers.test.checkers-test
  (:require [midje.sweet :refer :all]
            [dn.test-helpers.checkers :refer :all]))

(facts "about `html-element?`"
       (fact "true if the vector starts with the given element"
             [:a {:href "test"}] => (html-element? :a)
             [:p "Test paragraph"] =not=> (html-element? :a))

       (fact "correctly verifies tags with classes and ids"
             [:p.test "Test paragraph"] => (html-element? :p)
             [:p#test "Test paragraph"] => (html-element? :p))

       (fact "fails on nil"
             nil =not=> (html-element? :a)
             [] =not=> (html-element? :b)))

